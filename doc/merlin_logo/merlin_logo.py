import matplotlib.pyplot as plt
from matplotlib.patches import Wedge, Rectangle, Polygon
from matplotlib.collections import PatchCollection
from matplotlib.cm import get_cmap
import numpy as np
from matplotlib.lines import Line2D
import copy

def icon_lim(ax):
    ax.set_ylim(-1, 12)
    ax.set_xlim(-6.6, 6.5)

def spines_off(ax):
    ax.axes.spines['top'].set_visible(False)
    ax.axes.spines['bottom'].set_visible(False)
    ax.axes.spines['right'].set_visible(False)
    ax.axes.spines['left'].set_visible(False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_ylim(-6, 12)
    ax.set_xlim(-9, 9)

sidebar_logo_size = 200 #pixels
favicon_size = 32 #or 16 pixels. format .ico

my_dpi = 96

logo, ax_logo = plt.subplots(figsize=(200/my_dpi, 200/my_dpi), dpi=my_dpi)
icon, ax_icon = plt.subplots(figsize=(32/my_dpi, 32/my_dpi), dpi=my_dpi)

hat = np.array([[-3, 9], [-0.5, 10.5], [-0.5, 11], [-1.5, 11], [-1.5, 12],
    [1.5, 12], [1.5, 11], [0.5, 11], [0.5, 10.5], [3, 9]])

ec = 'grey'
cm = get_cmap('viridis')
c_face = cm(0.7)
c_smile = cm(0.8)
cm2 = get_cmap('inferno')
c_hat = cm2(0.6)

patches_logo = [
    Rectangle((-5, -1), 10, 10, color=c_face, ec=ec),
    Rectangle((-5, -1), 2, 2, color=c_face, ec=ec),
    Rectangle((-1, -1), 2, 2, color=c_face, ec=ec),
    Rectangle((3, -1), 2, 2, color=c_face, ec=ec),
    Rectangle((-5, 3), 2, 2, color=c_face, ec=ec),
    Rectangle((-1, 3), 2, 2, color=c_face, ec=ec),
    Rectangle((3, 3), 2, 2, color=c_face, ec=ec),
    Rectangle((-5, 7), 2, 2, color=c_face, ec=ec),
    Rectangle((-1, 7), 2, 2, color=c_face, ec=ec),
    Rectangle((3, 7), 2, 2, color=c_face, ec=ec),
    Rectangle((-3, 5), 2, 2, color=c_smile, ec=ec),
    Rectangle((1, 5), 2, 2, color=c_smile, ec=ec),
    Rectangle((1, 1), 2, 2, color=c_smile, ec=ec),
    Rectangle((-3, 1), 2, 2, color=c_smile, ec=ec),
    Rectangle((-1, 1), 2, 2, color=c_smile, ec=ec),
    Polygon(hat,color=c_hat),
    Wedge((0, -4), 4, 60, 120, color=c_smile)
]

patches_icon = []
for p in patches_logo[0:-1]:
    patches_icon.append(copy.copy(p))
    patches_icon[-1].set_ec(None)

for artist in patches_logo:
    ax_logo.add_artist(artist)

for artist in patches_icon:
    ax_icon.add_artist(artist)

spines_off(ax_logo)
spines_off(ax_icon)
logo.subplots_adjust(top=0.9, bottom=0, left=0.05, right=0.95)
icon.subplots_adjust(top=1, bottom=0, left=0.0, right=1)
icon_lim(ax_icon)
logo.savefig('logo.png', dpi=my_dpi, transparent=True)
icon.savefig('icon.png', dpi=my_dpi, transparent=True)
