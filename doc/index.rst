Welcome to Merlin interface's documentation!
==================================================

Python library for interfacing with a Medipix3 detector through the Merlin readout software, over the TCP/IP interface.

The source code is accessible at https://gitlab.com/fast_pixelated_detectors/merlin_interface.

Contributions, feature requests and bug reports are welcome on the `issues page <https://gitlab.com/fast_pixelated_detectors/merlin_interface/issues>`_.

.. image:: images/index/merlin_interface_example.png
    :scale: 99 %
    :align: center


.. toctree::
   install
   tutorial
   api_documentation
   related_projects
   :maxdepth: 2
   :caption: Contents:

Contributions from Magnus Nord funded by EPSRC via the project "Fast Pixel Detectors: a paradigm shift in STEM imaging" (Grant reference EP/M009963/1).

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
